(function () {
	"use strict";

	// imports
	var CommandEnum = com.dgsprb.quick.CommandEnum;
	var Quick = com.dgsprb.quick.Quick;
	var GameObject = com.dgsprb.quick.GameObject;
	var Scene = com.dgsprb.quick.Scene;
	var BaseTransition = com.dgsprb.quick.BaseTransition;
	var Rect = com.dgsprb.quick.Rect;

	var COLUMN_WIDTH = 20;			/* with of a column */
	var COLUMN_VARIATION = 50;		/* how much a column can bounce up or down */
	var PLAYER_SPEED = 5;			/* how much the player ship moves */
	var CAVE_DECAY = 2;				/* how much the cave will shrink over time */
	var CAVE_SPEED = 2;				/* how much the cave scrolls */

	// functions
	function main() {
		Quick.setName("Skel");
		Quick.setNumberOfLayers(2);
		Quick.init(function () { return new GameScene() });
	}

	// class GameScene extends Scene
	var GameScene = (function () {
		function GameScene() {
			Scene.call(this);
			var background = new Background();
			this.add(background);
			this.player = new Player();
			this.cave = [];
			var pos = 0;
			var previous = null;
			while (pos < Quick.getCanvasWidth() + COLUMN_WIDTH) {
				var wall = new Column(previous);
				wall.setRight(Quick.getCanvasWidth() + pos);
				pos += COLUMN_WIDTH;
				this.cave.push(wall);
				this.add(wall);
				previous = wall;
			}

			this.player.setLayerIndex(1);
			this.add(this.player);
		}; GameScene.prototype = Object.create(Scene.prototype);

		GameScene.prototype.getNext = function () {
			return new GameScene();
		};

		GameScene.prototype.getTransition = function () {
			return new BaseTransition();
		};

		GameScene.prototype.popWall = function () {
			this.cave.shift();
		};

		GameScene.prototype.addWall = function (wall) {
			this.cave.push(wall);
			this.add(wall);
		};

		GameScene.prototype.gameOver = function () {
			this.cave.forEach(function(elem, idx, array) {
				elem.stop();
				elem.top.stop();
				elem.bottom.stop();
			});
		};

		GameScene.prototype.speedUp = function () {
			this.cave.forEach(function(elem, idx, array) {
				elem.setSpeedX(-(CAVE_SPEED * 2));
			});
		};

		GameScene.prototype.speedDown = function () {
			this.cave.forEach(function(elem, idx, array) {
				elem.setSpeedX(-CAVE_SPEED);
			});
		};

		return GameScene;
	})();

	// class Background extends GameObject
	var Background = (function () {
		function Background() {
			GameObject.call(this);
			this.setColor("#101");
			this.setHeight(Quick.getCanvasHeight());
			this.setWidth(Quick.getCanvasWidth());
		}; Background.prototype = Object.create(GameObject.prototype);

		return Background;
	})();

	var Cave = (function () {
		function Cave() {
			GameObject.call(this);
			this.addTag('cave');
			this.setColor('#600613')
			this.setHeight(Quick.getCanvasHeight());
			this.setSolid();
		}; Cave.prototype = Object.create(GameObject.prototype);

		return Cave;
	})();

	var Column = (function () {
		function Column (previous) {
			GameObject.call(this);
			var top,
			    height;
			if (previous) {
				var top = previous.getTop() + (Quick.random(COLUMN_VARIATION) - (COLUMN_VARIATION / 2));
				if (top < 0) {
					top = 0;
				}
				height = previous.getHeight() - CAVE_DECAY;
				if (height < 32) {
					height = 32;
				}
			} else {
				top = Quick.random(Quick.getCanvasHeight() / 4);
				height = Quick.getCanvasHeight() / 2;
			}

			this.addTag('column');
			this.setHeight(height);
			this.setTop(top);
			this.setWidth(COLUMN_WIDTH);
			this.setColor('#1a1a1a');
			this.setLeft(Quick.getCanvasRight());
			this.setSpeedX(-CAVE_SPEED);
			this.setBoundary(new Rect(Quick.getCanvasWidth() % COLUMN_WIDTH, 0,
							 		  Quick.getCanvasWidth() * 2, Quick.getCanvasHeight()));
		}; Column.prototype = Object.create(GameObject.prototype);

		Column.prototype.init = function () {
			this.top = new Cave();
			this.top.setBottom(this.getTop() - 1);
			this.top.setSpeedX(this.getSpeedX());
			this.top.setWidth(this.getWidth());
			this.top.setLeft(this.getLeft());
			this.top.setSolid();
			this.top.addTag('cave');
			this.getScene().add(this.top);

			this.bottom = new Cave();
			this.bottom.setTop(this.getBottom() + 1);
			this.bottom.setSpeedX(this.getSpeedX());
			this.bottom.setWidth(this.getWidth());
			this.bottom.setLeft(this.getLeft());
			this.bottom.setSolid();
			this.bottom.addTag('cave');
			this.getScene().add(this.bottom);
		};

		Column.prototype.offBoundary = function () {
			this.expire();
			this.top.expire();
			this.bottom.expire();
			this.getScene().addWall(new Column(this));
			this.getScene().popWall();
			this.getScene().player.upScore();
		};

		return Column;
	})();

	// class Player extends GameObject
	var Player = (function () {
		function Player() {
			GameObject.call(this);
			this.controller = Quick.getController();
			this.setImageId("ship");
			this.setX(50);
			this.setY(Quick.getCanvasHeight() / 2);
			this.setSolid();
			this.isDead = false;
			this.score = 0;
		}; Player.prototype = Object.create(GameObject.prototype);

		Player.prototype.respond = function () {
			if (!this.isDead) {
				if (this.controller.keyDown(CommandEnum.UP) && this.getTop() > 0) {
					this.moveY(-PLAYER_SPEED);
				} else if (this.controller.keyDown(CommandEnum.DOWN) && this.getBottom() < Quick.getCanvasHeight()) {
					this.moveY(PLAYER_SPEED);
				} else if (this.controller.keyPush(CommandEnum.RIGHT)) {
					this.getScene().speedUp();
				} else if (this.controller.keyUp(CommandEnum.RIGHT)) {
					this.getScene().speedDown();
				}
			}
		};

		// override
		Player.prototype.update = function () {
			this.respond();
		};

		Player.prototype.onCollision = function (other) {
			if (other.hasTag('cave')) {
				this.getScene().gameOver();
			}
			this.isDead = true;
		};

		Player.prototype.upScore = function () {
			this.score += 1;
			document.getElementById('score').innerHTML = this.score;
		}

		return Player;
	})();

	main();
})();
